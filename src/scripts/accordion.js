// Hide all
let panels = $("#accordion-inner > .accordion > .accordion__body").hide(),
  panelContainer = $("#accordion-inner > .accordion"),
  // panels = $("#accordion-inner > .accordion").hide()
  images = $("#accordion__img > div").hide();

// Show first
panels.first().show();
panelContainer.first().addClass("active");
images.first().show();

// Click panel
$("#ourwork__accordion-container #accordion-inner .accordion  h3").click(function() {
    let panel = $(this).parent(),
      tabName = panel.attr("tab-name"),
      image = $("#" + tabName + "-img");

    // Hide other panels
    images.hide();
    panels.slideUp();
    panelContainer.removeClass("active");

    // Show Panel
    panel.find(".accordion__body").slideDown();
    panel.addClass("active");
    image.fadeIn();

    return false;
  });

// Click panel
$("#homepage__accordion-container .accordion  h3").click(function() {
  let panel = $(this).parent();

  if (!panel.hasClass("active")) {
    // Hide other panels
    // panels.fadeOut();
    panels.slideUp();
    panelContainer.removeClass("active");

    // to prevent jumping issue
    if (panel.hasClass("opened")) {
      panels.each(function() {
        $(this).css("height", $(this).height());
      });
    } else {
      panels.each(function() {
        $(this).css("height", $(this).height() + 10);
      });
    }

    // Show Panel
    // panel.find(".accordion__body").fadeIn();
    panel.find(".accordion__body").slideDown();
    panel.addClass("active");

    panel.addClass("opened");

    return false;
  }
});

// Click panel
$("#about__accordion-container .accordion  h3").click(function() {
  let panel = $(this).parent();

  // Hide other panels
  panels.slideUp();
  panelContainer.removeClass("active");

  // Show Panel
  panel.find(".accordion__body").slideDown();
  panel.addClass("active");

  return false;
});

// new accordion for our work page

$(document).ready(function() {
  var animTime = 350,
    clickPolice = false;

  $(document).on("touchstart click", ".acc-btn", function() {
    if (!clickPolice) {
      clickPolice = true;

      var currIndex = $(this).index(".acc-btn"),
        targetHeight = $(".acc-content-inner")
          .eq(currIndex)
          .outerHeight();

      $(".acc-btn h1").removeClass("selected");
      $(this)
        .find("h1")
        .addClass("selected");

      // only add ref link when that dropdown is opened
      $(".ref").removeClass("selected");
      // $('.ref').removeClass('ref-toggle');
      $(this)
        .next()
        .find(".ref")
        .addClass("selected");

      $(".acc-content")
        .stop()
        .animate({ height: 0 }, animTime);

      $(".acc-content")
        .eq(currIndex)
        .stop()
        .animate({ height: targetHeight }, animTime);

      setTimeout(function() {
        clickPolice = false;
      }, animTime);

      // $('.ref').hide();
      // setTimeout(function() {
      // 	$('.ref.selected').show();
      // }, 100);

      // setting up images

      // let panel = $(this),
      let tabName = $(this).attr("id"),
        image = $("#" + tabName + "-img");

      console.log("image name " + image);

      // Hide other panels
      images.hide();

      // Show Panel
      image.fadeIn();
    }
  });
});
