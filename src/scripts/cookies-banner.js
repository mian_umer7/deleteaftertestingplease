$(document).ready(function() {

    if(localStorage.getItem('cookieSeen') != 'shown'){
      // $(".cookie-banner").delay(100).fadeIn();
      $(".cookie-banner").fadeIn();
    }
    
    $('.close').click(function() {
      $('.cookie-banner').fadeOut(250); 
      localStorage.setItem('cookieSeen','shown');
    });
    
  });
  