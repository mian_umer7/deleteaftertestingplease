const TIMEOUT_FRAME = 1000 / 60;
const setTimeoutPolyfill = callback => {
	setTimeout(callback, TIMEOUT_FRAME);
};
const requestAnimationFrame = window.requestAnimationFrame || setTimeoutPolyfill;

const requestFrameDebounce = callback => {
	let isUpdating = false;
	
	const updateFrame = () => {
		callback();
		isUpdating = false;
	};

	return () => {
		if (isUpdating) return;

		isUpdating = true;
		requestAnimationFrame(updateFrame);
	};
};

const DESKTOP_BREAKPOINT = 800;
const HEADER_HEIGHT = 0;

export default containerEl => {
	const bagEl = containerEl.querySelector('.fixedScroller-fixed-side');

	const checkBagPosition = () => {
		const containerPosition = containerEl.getBoundingClientRect();
		const bagPosition = bagEl.getBoundingClientRect();
		const topPosition = (window.innerHeight - bagPosition.height + HEADER_HEIGHT) / 32;
		const maxTop = containerPosition.height - bagPosition.height - topPosition;

		// Fix the animation to the viewport on desktop
		if (window.innerWidth >= DESKTOP_BREAKPOINT) {
			if (containerPosition.top > topPosition) {
				bagEl.classList.remove('fixed');
				bagEl.style.top = '';
			} else if (
				containerPosition.top <= topPosition &&
				containerPosition.top >= -maxTop
			) {
				bagEl.classList.add('fixed');
				bagEl.style.top = `${topPosition}px`;
			} else if (
				containerPosition.top <= topPosition &&
				containerPosition.top < -maxTop
			) {
				bagEl.classList.remove('fixed');
				bagEl.style.top = `${maxTop + topPosition}px`;
			}
		} else {
			bagEl.classList.remove('fixed');
			bagEl.style.top = '';
		}
	};

	window.addEventListener('scroll', requestFrameDebounce(checkBagPosition));
	window.addEventListener('resize', requestFrameDebounce(checkBagPosition));
};