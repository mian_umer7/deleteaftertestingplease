import '../styles/main.scss';

import '../scripts/cd-slider.js';


import lazysizes from '../scripts/lazysizes.min.js';

//
// === Site Credit ===
//

console.log(
	'Site Credit' + '\n' +
	'===========' + '\n' +
	'Design & Development : https://alright.studio/'
);

//
// === Checking for Touch Devices ===
//

var checkMobile = function(){

  //Check Device
  var isTouch = ('ontouchstart' in document.documentElement);

  //Check Device //All Touch Devices
  if ( isTouch ) {

      $('html').addClass('touch');

  }
  else {

      $('html').addClass('no-touch');

  }

};

//Execute Check
checkMobile();

//
// === Custom Dropdown ===
//

// import '../scripts/custom-select.js';

var x, i, j, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName('custom-select');
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName('select')[0];
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement('DIV');
  a.setAttribute('class', 'select-selected');
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement('DIV');
  b.setAttribute('class', 'select-items select-hide');
  for (j = 1; j < selElmnt.length; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement('DIV');
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener('click', function() {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName('select')[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName('same-as-selected');
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute('class', 'same-as-selected');
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
  }

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);

//
// === Sticky Scroll Functionality ===
//

import fixedScroller from '../scripts/fixedScroller';
import forEach from 'lodash/forEach';

const fixedScrollContainer = document.querySelectorAll(
  '.fixedScroller-container'
);

forEach(fixedScrollContainer, fixedScroller);



//
// === Partner Menu functionality ===
//

$(function() {

  // clicking the menu open
  $('.partner-menu-button').click(function() {

    $('.partner-menu.clicked--open.open').removeClass('clicked--open open'); 
    $('.partner-menu-inner.clicked--open.open').removeClass('clicked--open open'); 

    if ( $('html').hasClass('touch') ) {
      $('.partner-menu').toggleClass('clicked--open'); 
      $('.partner-menu-inner').toggleClass('clicked--open');
    } else {
      if ( $('.partner-menu').hasClass('open') ) {
        $('.partner-menu').toggleClass('clicked--open'); 
        $('.partner-menu-inner').toggleClass('clicked--open');
      }
    }
    
  });

  // close partner menu if clicked anywhere else on the window

  $('html').click(function(e) {                    
      if(!$(e.target).hasClass('partner-menu-button') )
      {
        $('.partner-menu.clicked--open.open').removeClass('clicked--open open'); 
        $('.partner-menu-inner.clicked--open.open').removeClass('clicked--open open'); 
      }
  }); 
  
  // opening the menu on hover (OLD)
  $('.partner-menu-button').mouseover(function(e) {
    if ( $('html').hasClass("no-touch") ) {
      e.preventDefault();
      $('.partner-menu').addClass('open');
      $('.partner-menu-inner').addClass('open');
    }
  });

  // closing the menu when mouse leaves (OLD)
  $('.partner-menu').mouseleave(function(e) {
    if ( $('html').hasClass("no-touch") ) {
      e.preventDefault();

      if ( !$('.partner-menu').hasClass("clicked--open") ) {
        $('.partner-menu').removeClass('open');
        $('.partner-menu-inner').removeClass('open');
      }
    }
    
  });
  
});

var loc = window.location.pathname;

$('.partner-menu-inner').find('a').each(function() {
  $(this).toggleClass('active', $(this).attr('href') == loc);
});


//
// === Sticky Header functionality ===
//

import WOW from '../scripts/wow.min.js';

new WOW().init();

$(window).bind('scroll', function() {
  var distance = 500;
  if ($(window).scrollTop() > distance) {
    $('.header-sticky').removeClass('hidden');
    $('.header-sticky').addClass('animated fadeInDown');
  }
  else {
    $('.header-sticky').removeClass('animated fadeInDown');
    $('.header-sticky').addClass('hidden');
  }
});


// //
// // === Menu Takeover functionality ===
// //

$('.takeover-bg').hide(); // hide takeover BG Initially

$(function() {
  $('.menu').click(function(e) {
    e.preventDefault();

    // toggling takeover BG 
    if ( $('.menu-takeover').hasClass('open') ) {
      $('.takeover-bg').fadeOut();
    } else {
      $('.takeover-bg').fadeIn();
      $('html,body').animate({scrollTop:0},0);
    }

    $('.menu-takeover').toggleClass('open');

    $('.menu').toggleClass('open');
    
    

    $('body').toggleClass('noScroll');
    
    $(".header .menu-btn-bg").toggleClass('dn');
    $(".header .menu-link").toggleClass('dn');
    $(".header .partner-menu").toggleClass('dn');
  })
})

// //
// // === Sticky Slider Nav ===
// //

$(window).bind('scroll', function() {

  // // === scrolled to first section ===

  var top     = $(window).scrollTop();
  var offset = $('.row2').offset().top;
  var distance = (offset - top) - 300;

  if ($(window).scrollTop() < (distance + offset)) {

    $('.link-one').addClass('active');
    $('.link-two').removeClass('active');

  }
  else {
    $('.link-two').removeClass('active');    
  }

  // // === scrolled to second section ===

  if ($(window).scrollTop() > (distance + offset)) {

    $('.link-one').removeClass('active');
    $('.link-two').addClass('active');

  }
  else {
    $('.link-two').removeClass('active');    
  }

  // // === scrolled to third section ===

  var offsetTwo = $('.row3').offset().top;
  var distanceTwo = (offsetTwo - top) - 300;

  if ($(window).scrollTop() > (distanceTwo + offsetTwo)) {
    $('.link-two').removeClass('active');
    $('.link-three').addClass('active');
  }
  else {
    $('.link-three').removeClass('active');
  }


  // // === scrolled to fourth section ===

  var offsetThree = $('.row4').offset().top;
  var distanceThree = (offsetThree - top) - 300;

  if ($(window).scrollTop() > (distanceThree + offsetThree)) {
    $('.link-three').removeClass('active');
    $('.link-four').addClass('active');
  }
  else {
    $('.link-four').removeClass('active');
  }

    // // === scrolled to fifth section ===

  var offsetFour = $('.row5').offset().top;
  var distanceFour = (offsetFour - top) - 300;

  if ($(window).scrollTop() > (distanceFour + offsetFour)) {
    $('.link-four').removeClass('active');
    $('.link-five').addClass('active');
  }
  else {
    $('.link-five').removeClass('active');
  }
  
  
    // === scrolled to green section ===

    var offsetGreen = $('.green-section').offset().top;
    var distanceGreen = (offsetGreen - top) - 300;

    if ($(window).scrollTop() > (distanceGreen + offsetGreen)) {
      $('.scroller-navigation').removeClass('color--green');
      $('.scroller-navigation').addClass('color--white');
      
    }
    else {
      $('.scroller-navigation').removeClass('color--white');
      $('.scroller-navigation').removeClass('something');
    }

    // === scrolled to white section ===

    var offsetWhite = $('.white-section').offset().top;
    var distanceWhite = (offsetWhite - top) - 1000; // extra amount to account fot iPad pro portrait issue
    // var distanceWhite = (offsetWhite - top) - 300;

    if ($(window).scrollTop() > (distanceWhite + offsetWhite)) {
      $('.scroller-navigation').removeClass('color--white');
      $('.scroller-navigation').addClass('color--green');
    }
    else {
      $('.scroller-navigation').removeClass('color--green');
      $('.scroller-navigation').removeClass('v-white');

    }
  
});

// //
// // === Add smooth scrolling to all links ===
// //

$("a").on('click', function(event) {
  if (this.hash !== "") {
    
    event.preventDefault();
    var hash = this.hash;

    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 800, function(){
      window.location.hash = hash;
    });
  } // End if
});

// //
// // === Homepage Latest Content flickity custom buttons ===
// //

// init Flickity
var $homepagecarousel = $('.carousel-latest-content');
// Flickity instance
var flkty = $homepagecarousel.data('flickity');
// elements
var $cellButtonGroup = $('.button-group--cells');
var $cellButtons = $cellButtonGroup.find('.button');

// update selected cellButtons
$homepagecarousel.on( 'select.flickity', function() {
  $cellButtons.filter('.is-selected')
    .removeClass('is-selected');
  $cellButtons.eq( flkty.selectedIndex )
    .addClass('is-selected');
});

// select cell on button click
$cellButtonGroup.on( 'click', '.button', function() {
  var index = $(this).index();
  $homepagecarousel.flickity( 'select', index );
});
// previous
$('.button--previous').on( 'click', function() {
  $homepagecarousel.flickity('previous');
});
// next
$('.button--next').on( 'click', function() {
  $homepagecarousel.flickity('next');
});

 //
// // === Our Work scroller flickity custom buttons ===
// //

// init Flickity
var $ourworkcarousel = $('.carousel-ourwork');
// Flickity instance
var flktyourwork = $ourworkcarousel.data('flickity');
// elements
var $cellButtonGroupourwork = $('.button-group--cells');
var $cellButtonsourwork = $cellButtonGroupourwork.find('.button');

// update selected cellButtonsourwork
$ourworkcarousel.on( 'select.flickity', function() {
  $cellButtonsourwork.filter('.is-selected')
    .removeClass('is-selected');
  $cellButtonsourwork.eq( flktyourwork.selectedIndex )
    .addClass('is-selected');
});

// select cell on button click
$cellButtonGroupourwork.on( 'click', '.button', function() {
  var index = $(this).index();
  $ourworkcarousel.flickity( 'select', index );
});

// previous
$('.button--previous').on( 'click', function() {
  $ourworkcarousel.flickity('previous');
});

// next
$('.button--next').on( 'click', function() {
  $ourworkcarousel.flickity('next');
});

// hiding side arrows by default on first slide
$(".carousel-ourwork-buttons .button--previous").hide();

// showing side arrows only on slides other than the first
$('.ourwork-carousel-wrapper').on( 'click', function() {

  if( $('.first-slide').hasClass("is-selected") ) {
    console.log("first slide is elected");
    // $(".carousel-ourwork-buttons").hide();
    $(".button--previous").hide();
  } else {
    // $(".carousel-ourwork-buttons").show();
    $(".button--previous").show();
  }

  if( $('.last-slide').hasClass("is-selected") ) {
    console.log("last slide is elected");
    $(".button--next").hide();
  } else {
    $(".button--next").show();
  }

});


//
// // === Employers scroller flickity custom buttons ===
// //

// init Flickity
var $employerscarousel = $('.carousel-employers');
// Flickity instance
var flktyemployers = $employerscarousel.data('flickity');
// elements
var $cellButtonGroupemployers = $('.button-group--cells');
var $cellButtonsemployers = $cellButtonGroupemployers.find('.button');

// update selected cellButtonsemployers
$employerscarousel.on( 'select.flickity', function() {
  $cellButtonsemployers.filter('.is-selected')
    .removeClass('is-selected');
  $cellButtonsemployers.eq( flktyemployers.selectedIndex )
    .addClass('is-selected');
});

// select cell on button click
$cellButtonGroupemployers.on( 'click', '.button', function() {
  var index = $(this).index();
  $employerscarousel.flickity( 'select', index );
});
// previous
$('.button--previous').on( 'click', function() {
  $employerscarousel.flickity('previous');
});
// next
$('.button--next').on( 'click', function() {
  $employerscarousel.flickity('next');
});

// hiding previous arrows by default on first slide
$(".carousel-employers-buttons .button--previous").hide();

// showing side arrows only on slides other than the first
$('.employers__scroller').on( 'click', function() {

  if( $('.first-slide').hasClass("is-selected") ) {
    console.log("first slide is elected");
    // $(".carousel-employers-buttons").hide();
    $(".button--previous").hide();
  } else {
    // $(".carousel-employers-buttons").show();
    $(".button--previous").show();
  }

  if( $('.last-slide').hasClass("is-selected") ) {
    console.log("last slide is elected");
    $(".button--next").hide();
  } else {
    $(".button--next").show();
  }

});

//
// // === News & Resources flickity custom buttons ===
// //

// init Flickity
var $newscarousel = $('.carousel-news-wrapper');
// Flickity instance
var flktynews = $newscarousel.data('flickity');
// elements
var $cellButtonGroupnews = $('.button-group--cells');
var $cellButtonsnews = $cellButtonGroupnews.find('.button');

// update selected cellButtonsemployers
$newscarousel.on( 'select.flickity', function() {
  $cellButtonsnews.filter('.is-selected')
    .removeClass('is-selected');
  $cellButtonsnews.eq( flktynews.selectedIndex )
    .addClass('is-selected');
});

// select cell on button click
$cellButtonGroupnews.on( 'click', '.button', function() {
  var index = $(this).index();
  $newscarousel.flickity( 'select', index );
});
// previous
$('.button--previous').on( 'click', function() {
  $newscarousel.flickity('previous');
});
// next
$('.button--next').on( 'click', function() {
  $newscarousel.flickity('next');
});

//
// // === Careers flickity custom buttons ===
// //

// init Flickity
var $careerscarousel = $('.carousel-careers-news');
// Flickity instance
var flktycareers = $careerscarousel.data('flickity');
// elements
var $cellButtonGroupcareers = $('.button-group--cells');
var $cellButtonscareers = $cellButtonGroupcareers.find('.button');

// update selected cellButtonsemployers
$careerscarousel.on( 'select.flickity', function() {
  $cellButtonscareers.filter('.is-selected')
    .removeClass('is-selected');
  $cellButtonscareers.eq( flktycareers.selectedIndex )
    .addClass('is-selected');
});

// select cell on button click
$cellButtonGroupcareers.on( 'click', '.button', function() {
  var index = $(this).index();
  $careerscarousel.flickity( 'select', index );
});
// previous
$('.button--previous').on( 'click', function() {
  $careerscarousel.flickity('previous');
});
// next
$('.button--next').on( 'click', function() {
  $careerscarousel.flickity('next');
});


// //
// // === About page - read more expand functionality ===
// //
// 


$(document).ready(function() {

  
  // with media queries

  $(".eventDetails").hide();
  $(".details").html("Read More [+]");

  $(".details").click(function() {
    if (window.matchMedia('(max-width: 1200px)').matches) {
      
      // read more functionality for mobile (modal popup)
      $('.modal').toggleClass('modal--active');       
      var id = $(this).attr("href");
      var textWrapper = $(".eventDetails" + id).html();
      $(".modal__text").html(textWrapper);
      var heading = $(this).parent().parent().parent().find('.fw--800').html();
      $(".modal__headline").html(heading);
          
    } else {
      // read more functionality for desktop
      var myelement = $(this).attr("href")
      $(myelement).slideToggle("slow");
      $(this).toggleClass("open");
      $(".eventDetails:visible").not(myelement).hide();  

      $(".details").html("Read More [+]");
        
      if ( $(this).hasClass("open") ) {
          $(this).html("Read Less [-]");
      } else {
          $(this).html("Read More [+]");
      }
    }

  });


  // dropdown toggle
  
  $('.dropdown-toggle').on('click', function () {  
    $('.dropdown-container').toggleClass('dropdown-container--active');    
  });

  var dropdownOffset = $('.dropdown-toggle').offset();
  var leftOffset = dropdownOffset.left;

  console.log(leftOffset);

  // $('.dropdown').css("left", dropdownOffset.left);
  // $('.dropdown').css("left", leftOffset);


});

// open all News links in new tab
$(function(){
  $("#news-links a").attr("target","_blank");
});


$(function() {
	$('#indicator-img').matchHeight({
        target: $('#para-content')
    });
});

$(window).scroll(function(){
  if ($(this).scrollTop() < $('#head').scrollTop()) {
     $('#head').addClass('newClass');
  } else {
     $('#head').removeClass('newClass');
  }
});

$(".contact-link").on('click', function(){
  window.location.href = "/about#contact";    
});

$(".cookies-link").on('click', function(){
  window.location.href = "/privacy-policy#cookies";    
});

import '../scripts/custom-select.js';

$(document).ready(function() {


  var height = $('.homepage-accordion').innerHeight();
  $('.homepage-accordion').css("max-height", height);
  $('.homepage-accordion').css("min-height", height);

  $(window).on('resize', function(){
    var height = $('.homepage-accordion').innerHeight();
    $('.homepage-accordion').css("max-height", height);
    $('.homepage-accordion').css("min-height", height);
  });
});



// modal stuff

$('.toggleModal').on('click', function () {  
  $('.modal').toggleClass('modal--active');    
  // $('.modalContainer').toggleClass('modalContainer--active');    
});

$('.ref-toggle').on('click', function () {  
  if( !$(this).hasClass("close") ) {
    $('.modal--ref').removeClass('modal--active');    
  }
});

if ( $('html').hasClass("touch") ) {
  // $('.home-acc-desktop').hide();
  $('.home-acc-mobile').hide();
} else {
  $('.home-acc-mobile').hide();
}

import '../scripts/accordion.js';

$('#homepage__accordion-container .acc-btn').on('click', function () {  
  // $(this).next().find('.selected.ref').addClass('ref-toggle');
  setTimeout(function() {
    $(this).next().find('.ref').addClass('clicked-open-boi');
  }, 100);    
});

// reference popup modals 

$('.ref-toggle#ref-1').on('click', function () {  
  $('.modal#ref-1').toggleClass('modal--active');    
});
$('.ref-toggle#ref-2').on('click', function () {   
  $('.modal#ref-2').toggleClass('modal--active');    
});
$('.ref-toggle#ref-3').on('click', function () {  
  $('.modal#ref-3').toggleClass('modal--active');    
});
$('.ref-toggle#ref-4').on('click', function () {  
  $('.modal#ref-4').toggleClass('modal--active');    
});
$('.ref-toggle#ref-5').on('click', function () {  
  $('.modal#ref-5').toggleClass('modal--active');    
});
$('.ref-toggle#ref-6').on('click', function () {  
  $('.modal#ref-6').toggleClass('modal--active');    
});
$('.ref-toggle#ref-7').on('click', function () {  
  $('.modal#ref-7').toggleClass('modal--active');    
});
$('.ref-toggle#ref-8').on('click', function () {  
  $('.modal#ref-8').toggleClass('modal--active');    
});
$('.ref-toggle#ref-9').on('click', function () {  
  $('.modal#ref-9').toggleClass('modal--active');    
});
$('.ref-toggle#ref-10').on('click', function () {  
  $('.modal#ref-10').toggleClass('modal--active');    
});

// custom tab order for custom select box in forms

$('.about__form .select-selected').attr("tabindex", "0");

$('html.no-touch .about__form select').attr("tabindex", "-9999999"); // hidden select element out of tab order

$('.about__form .select-items').click(function() {
  $(this).prev().addClass('custom-color');
});

$('.providers__form .select-selected').attr("tabindex", "0");
$('html.no-touch .providers__form select').attr("tabindex", "-9999999"); // hidden select element out of tab order

$('.providers__form .select-items').click(function() {
  $(this).prev().addClass('custom-color');
});

import '../scripts/modernizr-custom.js';

//add simple support for background images:
document.addEventListener('lazybeforeunveil', function(e){


  var bg = e.target.getAttribute('data-bg');
  var bg2 = e.target.getAttribute('data-bg2');
  if(bg){

      $('<img>').attr('src', bg).load(function(){
      e.target.style.backgroundImage = 'url(' + bg + ')';
      }).error(function(){
        e.target.style.backgroundImage = 'url(' + bg2 + ')';
        
      });
     
  }
});

//or add AJAX loading
//<div class="lazyload" data-ajax="my-url.html"></div>

// $(document).on('lazybeforeunveil', function(){
// var ajax = $(e.target).data('ajax');
//   if(ajax){
//       $(e.target).load(ajax);
//   }
// });




