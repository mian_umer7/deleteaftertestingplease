require('dotenv').config();

const path = require('path');
const fs = require('fs');
const reduce = require('lodash/reduce');
const autoprefixer = require('autoprefixer');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

//
// === Constants ===
//

const ROOT_DIR = path.resolve(__dirname, '..');
const ENTRYPOINT_DIR = path.resolve(ROOT_DIR, 'src', 'entrypoints');
const DIST_DIR = path.resolve(ROOT_DIR, 'web', 'dist');

//
// === Get entrypoints from folder ===
//

const entrypointFileNames = fs.readdirSync(ENTRYPOINT_DIR).filter(file => {
	return file.match(/.*\.js$/);
});

const entrypoints = reduce(entrypointFileNames, (entrypoints, fileName) => {
	const entrypointName = fileName.replace('.js', '');

	return {
		...entrypoints,
		[entrypointName]: path.resolve(ENTRYPOINT_DIR, fileName),
	};
}, {});

//
// === Sass Loader Options ===
//

const postCSSOptions = {
	plugins: () => [
		autoprefixer()
	]
};

const sassLoaderOptions = {
	includePaths: [
		path.resolve(ROOT_DIR, 'node_modules'),
	]
};

const sassLoader = {
	test: /\.(scss|css)$/,
	use: [
		'style-loader',
		'css-loader',
		{
			loader: 'postcss-loader',
			options: postCSSOptions
		},
		{
			loader: 'sass-loader',
			options: sassLoaderOptions
		}
	],
	include: path.resolve(ROOT_DIR, 'src')
};

//
// === JS Loaders ===
//

const eslintLoader = {
	test: /\.jsx?$/,
	loader: 'eslint-loader',
	enforce: 'pre',
	exclude: path.resolve(ROOT_DIR, 'node_modules')
};

const jsxLoader = {
	test: /\.jsx?$/,
	loader: 'babel-loader',
	include: path.resolve(ROOT_DIR, 'src')
};

//
// === Fonts & Images ===
//

const fontImageLoader = {
	test: /\.(woff(2)?|ttf|otf|eot|png|jpg|jpeg|svg|gif)$/,
	loader: 'file-loader',
};

//
// === Config ===
//

module.exports = {
	entry: entrypoints,
	mode: 'development',
	output: {
		path: DIST_DIR,
		publicPath: `${process.env.ASSET_HOST}/dist/`,
		filename: '[name].js',
		chunkFilename: '[name].[chunkhash].js'
	},
	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			react: 'preact-compat',
			'react-dom': 'preact-compat',
		},
	},
	module: {
		rules: [
			eslintLoader,
			jsxLoader,
			sassLoader,
			fontImageLoader,
		]
	},
	plugins: [
		new BrowserSyncPlugin({
			host: '127.0.0.1',
			port: process.env.BROWSERSYNC_PORT,
			proxy: 'http://covera.local',
		}, {
			reload: true,
			injectCss: true,
		})
	],
	devServer: {
		hot: false,
		inline: false,
		port: process.env.WEBPACK_PORT,
		contentBase: path.resolve(ROOT_DIR, 'web'),
		allowedHosts: [
			'kindred.local'
		],
		headers: {
			'Access-Control-Allow-Origin': '*'
		}
	}
};