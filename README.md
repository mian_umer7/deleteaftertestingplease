# Covera Health CraftCMS Site

## Requirements
- MAMP
- Node 8+
- Yarn
- Composer
- PHP 5+
- MySQL 5+

## Local Development

*Local development requires MAMP.*

1. Install Composer and Yarn dependencies
```
$ composer install
$ yarn install
```
2. Start MAMP and create a new host (`covera.local`) and a database on `localhost`. Database backups can be found in Dropbox.
3. Copy `.env.example` to `.env` and set proper database configuration.
4. Start up the Browsersync for live reloading changes:
```
$ yarn start
```
5. Navigate to your host (`http://covera.local`) in the browser.


## Deploying to Production

1. Add Fortrabbit as a GIT remote, named `production`.
2. Run the `deploy.sh` script. Your GIT password is the fortrabbit account password.

```
$ sh scripts/deploy.sh
```

This will do a few things - compress assets, commit those as a new deployment to the repo, and push to the `production` branch.


## Deploying to Staging

1. Add Fortrabbit as a GIT remote, named `staging`.
2. Run the `deploy.sh` script. Your GIT password is the fortrabbit account password.

```
$ sh scripts/deploy-staging.sh
```

This will do a few things - compress assets, commit those as a new deployment to the repo, and push to the `production` branch.
